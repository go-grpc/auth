package main

import (
	grpc2 "auth/internal/adapter/grpc"
	"auth/internal/domain/auth/proto"
	"auth/internal/domain/auth/repository"
	"auth/internal/domain/auth/usecase"
	"auth/pkg/client/postgresql"
	"context"
	"github.com/joho/godotenv"
	"google.golang.org/grpc"
	"net"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	err := godotenv.Load(".env")
	if err != nil {
		panic(err)
	}
	client, err := postgresql.NewClient(context.Background(), 5, os.Getenv("POSTGRES_USER"),
		os.Getenv("POSTGRES_PASSWORD"), os.Getenv("POSTGRES_HOST"), os.Getenv("POSTGRES_PORT"), os.Getenv("POSTGRES_DB"))
	if err != nil {
		panic("helloooo")
	}
	listener, err := net.Listen("tcp", ":3006")
	r := repository.NewRepository(client)
	userConn, err := grpc.Dial(":"+os.Getenv("PORT"), grpc.WithInsecure())
	if err != nil {
		panic("user service not connect")
	}
	userClient := proto.NewUserServiceClient(userConn)
	us := usecase.NewUseCase(r, userClient)
	srv := grpc.NewServer()
	grpc2.NewAuthServerGRPC(srv, us)
		if err := srv.Serve(listener); err != nil {
			panic(err)
		}
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)

	<-quit
	println("server started")
}
