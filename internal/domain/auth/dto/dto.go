package dto

type RegistrationDto struct {
	Phone string `json:"phone"`
	Password string `json:"password"`
}
type LoginDto struct {
	Phone string `json:"phone"`
	Password string `json:"password"`
}
type CreateTokenDto struct {
	UserId int `json:"userId"`
	Token string `json:"token"`
}

type LoginANDRegistrationResponse struct {
	AccessToken string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
}