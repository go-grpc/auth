package repository

import (
	"auth/internal/domain/auth/dto"
	"auth/pkg/client/postgresql"
	"context"
	"fmt"
)

type Repository struct {
	client postgresql.Client
}

func (r Repository) DeleteToken(ctx context.Context, token string) error {
	q := `DELETE FROM tokens WHERE token = $1`
	_, err := r.client.Query(ctx, q, token)
	if err != nil {
		return err
	}
	return nil
}

func (r Repository) GetTokenByUserId(ctx context.Context, userId int) (int, error) {
	var id int
	q := `SELECT id FROM tokens WHERE user_id = $1`
	rows, err := r.client.Query(ctx, q, userId)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		if err = rows.Scan(&id); err != nil {
			return 0, err
		}
	}
	return userId, err
}

func (r Repository) CreateToken(ctx context.Context, dto dto.CreateTokenDto) (int, error) {
	var id int
	q := `INSERT INTO tokens (user_id, token) VALUES ($1,$2) RETURNING id`
	rows, err := r.client.Query(ctx, q, dto.UserId, dto.Token)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		if err = rows.Scan(&id); err != nil {
			return 0, err
		}
	}
	return id, nil
}

func (r Repository) GetToken(ctx context.Context, token string) (int, error) {
	var id int
	q := `SELECT id FROM tokens where token = $1`
	rows, err := r.client.Query(ctx, q, token)
	fmt.Println(err)
	if err != nil {
		return 0, err
	}
	for rows.Next() {
		if err = rows.Scan(&id); err != nil {
			return 0, err
		}
	}
	return id, err
}

//func (r Repository) CreateUser(ctx context.Context, dto dto.RegistrationDto) (int, error) {
//	var id int
//	q := `INSERT INTO  users (phone, password) VALUES ($1,$2) RETURNING id`
//	rows, err := r.client.Query(ctx, q, dto.Phone, dto.Password)
//	if err != nil {
//		return 0, err
//	}
//	for rows.Next() {
//		if err := rows.Scan(&id); err != nil {
//			return 0, nil
//		}
//	}
//	return id, nil
//}
//
//func (r Repository) GetUser(ctx context.Context, loginDto dto.LoginDto) (auth.User, error) {
//	var u auth.User
//	q := `SELECT id,phone,password FROM users WHERE phone = $1`
//	rows, err := r.client.Query(ctx, q, loginDto.Phone)
//	if err != nil {
//		return auth.User{}, err
//	}
//	for rows.Next() {
//		if err = rows.Scan(&u.Id, &u.Phone, &u.Password); err != nil {
//			return auth.User{}, err
//		}
//	}
//	return u, nil
//}

func NewRepository(client postgresql.Client) IRepository {
	return &Repository{client: client}
}
