package repository

import (
	"auth/internal/domain/auth/dto"
	"context"
)

type IRepository interface {
	GetToken(ctx context.Context,token string) (int,error)
	GetTokenByUserId(ctx context.Context,userId int) (int,error)
	CreateToken(ctx context.Context,dto dto.CreateTokenDto) (int,error)
	DeleteToken(ctx context.Context,token string) error
}
