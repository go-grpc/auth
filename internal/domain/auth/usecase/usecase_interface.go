package usecase

import (
	"auth/internal/domain/auth/dto"
	"context"
)

type IUseCase interface {
	Login(ctx context.Context, dto dto.LoginDto) (dto.LoginANDRegistrationResponse, error)
	Registration(ctx context.Context, dto dto.LoginDto) (dto.LoginANDRegistrationResponse, error)
	generateToken(userId int) (string, error)
	Refresh(ctx context.Context, token string) (dto.LoginANDRegistrationResponse, error)
	Logout(ctx context.Context, token string)
	saveToken(ctx context.Context, userId int, refreshToken string)
	getTokens(userId int) (dto.LoginANDRegistrationResponse, error)
	ParsToken(accessToken string) (int, error)
}
