package usecase

import (
	"auth/internal/domain/auth/dto"
	"auth/internal/domain/auth/proto"
	"auth/internal/domain/auth/repository"
	"context"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"strconv"
	"time"
)

const (
	salt       = "goifdighfs1234dgfsdjnosd"
	signingKey = "453645%$^*^&*&8gfdjlgfdslsfdgfds"
	tokenTTL   = 12 * time.Hour
)

type tokenClaims struct {
	jwt.StandardClaims
	UserId int `json:"id"`
}
type user struct {
	UserId int `json:"id"`
}
type UseCase struct {
	r  repository.IRepository
	us proto.UserServiceClient
}

func (u UseCase) ParsToken(accessToken string) (int, error) {
	fmt.Println(accessToken)
	token, err := jwt.ParseWithClaims(accessToken, &tokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("invalid signing method")
		}

		return []byte(signingKey), nil
	})
	if err != nil {
		return 0, err
	}

	claims, ok := token.Claims.(*tokenClaims)
	if !ok {
		return 0, errors.New("token claims are not of type *tokenClaims")
	}

	return claims.UserId, nil
}

func (u UseCase) getTokens(userId int) (dto.LoginANDRegistrationResponse, error) {
	accessToken, err := u.generateToken(userId)
	refreshToken, err := u.generateToken(userId)
	if err != nil {
		return dto.LoginANDRegistrationResponse{}, err
	}
	return dto.LoginANDRegistrationResponse{
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}, nil
}

func (u UseCase) Login(ctx context.Context, d dto.LoginDto) (dto.LoginANDRegistrationResponse, error) {
	candidate, err := u.us.GetUserForLogin(ctx, &proto.GetUserForCheck{Phone: d.Phone})
	if err != nil {
		return dto.LoginANDRegistrationResponse{}, err
	}
	if candidate.Id == 0 {
		return dto.LoginANDRegistrationResponse{}, errors.New("email или пароль не правильный")
	}
	err = bcrypt.CompareHashAndPassword([]byte(candidate.Password), []byte(d.Password))
	if err != nil {
		return dto.LoginANDRegistrationResponse{}, errors.New("email или пароль не правильный")
	}
	tokens, _ := u.getTokens(int(candidate.Id))
	u.saveToken(ctx, int(candidate.Id), tokens.RefreshToken)
	return tokens, err

}

func (u UseCase) Registration(ctx context.Context, d dto.LoginDto) (dto.LoginANDRegistrationResponse, error) {

	candidate, err := u.us.GetUserForLogin(ctx, &proto.GetUserForCheck{Phone: d.Phone})
	if err != nil {
		return dto.LoginANDRegistrationResponse{}, err
	}

	if candidate.Id != 0 {
		return dto.LoginANDRegistrationResponse{}, errors.New("такой телефон уже есть")
	}
	hashPassword, err := bcrypt.GenerateFromPassword([]byte(d.Password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err)
	}
	if err != nil {
		return dto.LoginANDRegistrationResponse{}, err
	}
	id, err := u.us.CreateUser(ctx, &proto.CreateUserRequest{
		Phone:    d.Phone,
		Password: string(hashPassword),
	})
	if err != nil {
		return dto.LoginANDRegistrationResponse{}, err
	}
	tokens, _ := u.getTokens(int(id.Id))
	u.saveToken(ctx, int(id.Id), tokens.RefreshToken)
	return tokens, err
}

func (u UseCase) generateToken(userId int) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(tokenTTL).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
		UserId: user{UserId: userId}.UserId,
	})
	return token.SignedString([]byte(signingKey))
}

func (u UseCase) Refresh(ctx context.Context, token string) (dto.LoginANDRegistrationResponse, error) {
	if token == "" {
		return dto.LoginANDRegistrationResponse{}, errors.New("вы не зарегестрированы")
	}
	fmt.Println(token)
	fmt.Println("token")
	verifyToken, err := u.ParsToken(token)
	if err != nil {
		fmt.Println("err in parse")
		return dto.LoginANDRegistrationResponse{}, err
	}
	refreshTokenFromDB, err := u.r.GetToken(ctx, token)
	fmt.Println(verifyToken)
	fmt.Println(refreshTokenFromDB)
	if verifyToken == 0 || refreshTokenFromDB == 0 {
		fmt.Println("err from two if")
		return dto.LoginANDRegistrationResponse{}, errors.New("вы не загерестрированы")
	}
	user, err := u.us.GetUserById(ctx, &proto.GetUserByIdRequest{UserId: int64(verifyToken)})
	if err != nil {
		fmt.Println("err from get userBy id")
		return dto.LoginANDRegistrationResponse{}, errors.New("что то пошло не так")
	}
	tokens, err := u.getTokens(int(user.UserId))
	if err != nil {
		fmt.Println("err in tokens")
		return dto.LoginANDRegistrationResponse{}, errors.New("что то пошло не так c токеном")
	}
	u.saveToken(ctx, int(user.UserId), tokens.RefreshToken)
	return dto.LoginANDRegistrationResponse{
		AccessToken:  tokens.AccessToken,
		RefreshToken: tokens.RefreshToken,
	}, nil
}

func (u UseCase) Logout(ctx context.Context, token string) {
	_ = u.r.DeleteToken(ctx, token)
}

func (u UseCase) saveToken(ctx context.Context, userId int, refreshToken string) {
	_, _ = u.r.CreateToken(ctx, dto.CreateTokenDto{
		UserId: userId,
		Token:  refreshToken,
	})
}

func NewUseCase(r repository.IRepository, us proto.UserServiceClient) IUseCase {
	return UseCase{r, us}
}

func stringToInt(s string) (int, error) {
	i, err := strconv.Atoi(s)
	if err != nil {
		return 0, err

	}
	return i, nil
}
