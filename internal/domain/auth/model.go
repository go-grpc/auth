package auth

type User struct {
	Id int `json:"id"`
	Phone string `json:"phone"`
	Password string `json:"password"`
}

type Auth struct {
	Id int `json:"id"`
	Token string `json:"token"`
	UserId int `json:"userId"`
}
type Role struct {
	Id int `json:"id"`
	Value string `json:"value"`
	Description string `json:"description"`
}
