package grpc

import (
	"auth/internal/adapter/grpc/proto"
	"auth/internal/domain/auth/dto"
	"auth/internal/domain/auth/usecase"
	"context"
	"fmt"
	"google.golang.org/grpc"
)

func NewAuthServerGRPC(gServer *grpc.Server, us usecase.IUseCase) {
	server := server{us}
	proto.RegisterAuthServiceServer(gServer, &server)
}

type server struct {
	u usecase.IUseCase
}

func (s server) Logout(ctx context.Context, request *proto.LogoutRequest) (*proto.LogoutResponse, error) {
	s.u.Logout(ctx, request.GetRefreshToken())
	return &proto.LogoutResponse{}, nil
}

func (s server) Refresh(ctx context.Context, request *proto.RefreshRequest) (*proto.RefreshResponse, error) {
	tokens, err := s.u.Refresh(ctx, request.RefreshToken)
	if err != nil {
		return &proto.RefreshResponse{}, err
	}
	return &proto.RefreshResponse{AccessToken: tokens.AccessToken, RefreshToken: tokens.RefreshToken}, nil
}

func (s server) ParseToken(_ context.Context, request *proto.ParsTokenRequest) (*proto.ParsTokenResponse, error) {
	userId, err := s.u.ParsToken(request.Token)
	if err != nil {
		return nil, err
	}
	fmt.Println(userId)
	return &proto.ParsTokenResponse{UserId: int32(userId)}, nil
}

func (s server) Login(ctx context.Context, request *proto.AuthRequest) (*proto.AuthResponse, error) {
	d := dto.LoginDto{
		Phone:    request.GetPhone(),
		Password: request.GetPassword(),
	}
	fmt.Println(d)
	t, err := s.u.Login(ctx, d)
	if err != nil {
		return nil, err
	}
	return &proto.AuthResponse{
		AccessToken:  t.AccessToken,
		RefreshToken: t.RefreshToken,
	}, nil

}

func (s server) Registration(ctx context.Context, request *proto.AuthRequest) (*proto.AuthResponse, error) {
	fmt.Println(request.Phone)
	d := dto.LoginDto{
		Phone:    request.GetPhone(),
		Password: request.GetPassword(),
	}
	t, err := s.u.Registration(ctx, d)
	if err != nil {
		return nil, err
	}
	return &proto.AuthResponse{
		AccessToken:  t.AccessToken,
		RefreshToken: t.RefreshToken,
	}, nil
}
