DROP TABLE IF EXISTS tokens;
DROP TABLE IF EXISTS users;

CREATE TABLE users
(
    id       serial PRIMARY KEY,
    phone    varchar(15) unique not null,
    password varchar            not null
);

CREATE TABLE tokens
(
    id      serial PRIMARY KEY,
    user_id int  not null,
    token   text not null,
    CONSTRAINT user_fk FOREIGN KEY (user_id) REFERENCES users (id)
);

